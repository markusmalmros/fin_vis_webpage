var ratio_mode = null;
MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});

function whiten(){
	opacity = "0.1";
	$(".h_class").css("opacity", opacity);
    $(".statement_headings").css("opacity", opacity);
    $(".rect_class").css("opacity", opacity);
    $("#h_equal").css("opacity", opacity);
    $(".rect_text").css("opacity", opacity);
    $("li.bullets").css("opacity", opacity);
    $('#opacity_div').show();
}
function unwhiten(){
	$(".h_class").css("opacity", "1");
    $(".statement_headings").css("opacity", "1");
	$(".rect_class").css("opacity", "1");
	$("#h_equal").css("opacity", "1");
	$(".rect_text").css("opacity", "0.7");
	$(".bullets").css("opacity", "1");	
	$('#opacity_div').hide();
	ratio_mode = null;
}


// Define colors
rev_color = "#9be8a3";
exp_color = "#9be8a3";
net_color = "#9be8a3";
asset_curr_color = "#aee2ef";
asset_lt_color = "#5abae2";
liab_curr_color = "#e098aa";
liab_lt_color = "#cc617c";
liab_eqt_color = "#857fcf";
cf_op_color = "#9be8a3";
cf_inv_color = "#9be8a3";
cf_fin_color = "#9be8a3";
mc_color = "#f7ca62";
neg_color = "#ef697b";

// Define financial variables
var unit = "million USD";
var revenue_tot = 215000;
var revenue = [{"Total Sales": revenue_tot}];
var revenue_color = [{"Total Sales": rev_color}];
var expenses_tot = -170000;
var expenses = [{"Cost of Revenue":-131000, "R&D":-10000, "Sales, General & Admin":-14000, "Additional Items":-1000, "Tax":-15000}];
var expenses_color = [{"Cost of Revenue":exp_color, "R&D":exp_color, "Sales, General & Admin":exp_color, "Additional Items":exp_color, "Tax":exp_color}];
var net_income_tot = 45000;
var net_income = [{"Net Income":  net_income_tot}];
var net_income_color = [{"Net Income":  net_color}];
var tot_assets = 321000;
var assets = [{"Cash & Cash Equivalents": 20000, "Short-Term Investments": 46000, "Net Receivables":29000, "Inventory": 2000, "Other Current Assets": 8000},{"Long-Term Investments": 170000, "Fixed Assets": 27000, "Goodwill": 5000, "Intangible Assets": 3000, "Other Assets": 9000}];
var assets_color = [{"Cash & Cash Equivalents": asset_curr_color, "Short-Term Investments": asset_curr_color, "Net Receivables":asset_curr_color, "Inventory": asset_curr_color, "Other Current Assets": asset_curr_color},{"Long-Term Investments": asset_lt_color, "Fixed Assets": asset_lt_color, "Goodwill": asset_lt_color, "Intangible Assets": asset_lt_color, "Other Assets": asset_lt_color}];
var eqt_tot = 128000
var liabilities_tot = 193000 + eqt_tot; 
var liabilities = [{"Accounts Payable":59000, "Short-Term Debt": 12000, "Other Current Liabilities": 8000}, {"Long-Term Debt": 75000, "Other Liabilities":36000, "Deferred Liability Charges": 3000}, {"Common Stocks": 31000, "Retained Earnings": 96000, "Other Equity": 0.6}];
var liabilities_color = [{"Accounts Payable":liab_curr_color, "Short-Term Debt": liab_curr_color, "Other Current Liabilities": liab_curr_color}, {"Long-Term Debt": liab_lt_color, "Other Liabilities":liab_lt_color, "Deferred Liability Charges": liab_lt_color}, {"Common Stocks": liab_eqt_color, "Retained Earnings": liab_eqt_color, "Other Equity": liab_eqt_color}];
var cf_operations_tot = 65000;
var cf_operations = [{"Net Income": net_income_tot, "Depreciation": 10000, "Net Income Adjustments": 9000}];
var cf_operations_color = [{"Net Income": cf_op_color, "Depreciation": cf_op_color, "Net Income Adjustments": cf_op_color}];
var cf_investing_tot = -46000;
var cf_investing = [{"Capital Expenditures": -12000, "Investments": -32000, "Other Investing Activities":-1000}];
var cf_investing_color = [{"Capital Expenditures": cf_inv_color, "Investments": cf_inv_color, "Other Investing Activities":cf_inv_color}];
var cf_financing_tot = -52500; // TODO: Solve the problem of positive and negative entries mixed
var cf_financing = [{"Sale and Purchase of Stock":-29000, "Net Borrowings": 22000, "Other Financing Activities": -1500}];
var cf_financing_color = [{"Sale and Purchase of Stock":cf_fin_color, "Net Borrowings": cf_fin_color, "Other Financing Activities": cf_fin_color}];
var shares_outstanding = 5214;
var price = 146;
var market_cap_tot = shares_outstanding*price;
var market_cap = [{"Market Capitalization": market_cap_tot}]
var market_cap_color = [{"Market Capitalization": mc_color}]


// TODO: Have to add if when determining screen width (dt or mobile?)
function positioning(){
	
	var window_height = $(window).height()-50;
	var window_width = $(window).width();
	
	if(window_width>768){
		var inner_height = Math.round((window_height) / 2);
		var inner_width = Math.round((window_width) / 2);
	}else{
		var inner_height = Math.round((window_height) / 4);
		var inner_width = Math.round((window_width));	
	}
	
	var str_c_h = String(parseInt(inner_height));
	var box_height = str_c_h.concat("px");
	$(".inner_container").css("height", box_height);
	
	
	
	// Define and calculate size variables
	var w_rev = math.sqrt(math.abs(revenue_tot))
	var w_exp = math.sqrt(math.abs(expenses_tot));
	var w_net = math.sqrt(math.abs(net_income_tot));
	var w_ass = math.sqrt(tot_assets);
	var w_lia = math.sqrt(liabilities_tot);
	var w_cf_op = math.sqrt(math.abs(cf_operations_tot));
	var w_cf_inv = math.sqrt(math.abs(cf_investing_tot)); 
	var w_cf_fin = math.sqrt(math.abs(cf_financing_tot)); 
	var w_mc = math.sqrt(market_cap_tot);
	
	// Set calculate the size restrictions
	var width_set = [(w_rev+w_exp+w_net), (w_ass+w_lia), (w_cf_op+w_cf_inv+w_cf_fin), (w_mc)];
	var height_set = [w_rev, w_exp, w_ass, w_cf_op, w_cf_inv, w_cf_fin, w_mc];
	var max_width = math.max(width_set);
	var max_height = math.max(height_set);
	
	// Find the axis with the least amount of space
	if(max_width/inner_width > max_height/inner_height){
		max_set = max_width;
		inner_dim = inner_width;
	}else{
		max_set = max_height;
		inner_dim = inner_height;
	} 
	
	// Maximum fraction of containers that can be filled with boxes, either width or height (depends on which axis has the most space)
	max_fill_space = 0.65;
	
	var width_matrix = math.matrix([[1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, w_net + w_rev + w_exp], 
					   [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, (w_ass + w_lia)], 
					   [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, w_cf_fin + w_cf_inv + w_cf_op],
					   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, w_mc],
					   [1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					   [0, 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					   [-2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					   [0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0],
					   [0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0],
					   [0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0, 0],
					   [0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0, 0, 0, 0],
					   [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, -1, 0, 0, 0],
					   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, 0],
					   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, max_set]]);
		
	var width_vect = math.matrix([inner_width, inner_width, inner_width, inner_width, 0, 0, 0, 0, 0, 0, 0, 0, 0, max_fill_space*inner_dim]);
	
	var x = math.lusolve(width_matrix, width_vect);
	
	var scale = math.subset(x, math.index(13,0));

    
    function create_fin_rect(fin_json, pix_width, svg_id, outer_div_id, left_offset, scale, div_class, tot_money, color_json){
    
	    $(svg_id).css("width", String(pix_width+3));
		$(svg_id).css("height", String(pix_width+3));
		$(svg_id).css("left", String(left_offset));
		
		$(svg_id).empty();
		
		var pos_x = 1;
		var pos_y = 1;
		var ix = 0;
		var id_arr = [];
		
		// Iterate each column
		for (var i = 0; i < fin_json.length; i++) {
		
			var fin_values = fin_json[i];
			var color_values = color_json[i];
			
			col_tot = 0.0;
			for (var fin_key in fin_values){ 
				col_tot = col_tot + math.abs(fin_values[fin_key]);
			}
			
			col_width = pix_width*math.abs(col_tot)/math.abs(tot_money);
			
			// Iterate each row
			for (var fin_key in fin_values){
			
		        var fin_value = math.abs(fin_values[fin_key]);
		        var box_color = color_values[fin_key];
		        var visual_value = fin_values[fin_key];
				var pix_height = fin_value*scale*scale/col_width;
				
				var id = String(String(ix)+div_class);
								
		        // Find fill color
		        if(visual_value > 0){
			        var fill_color = box_color;
		        }else{
			        var fill_color = neg_color;
		        }
		        	
				$(svg_id).append('<g><rect class="'+div_class+' rect_class" id="'+id+'" x="'+pos_x+'" y="'+pos_y+'" width="'+col_width+'" height="'+pix_height+'" stroke="black" stroke-width="1" fill="'+fill_color+'"> <title>'+fin_key+': $'+String(visual_value)+'M'+'</title> </rect> <text class="rect_text" id='+id+'text'+' x="'+String(pos_x)+'" y="'+String(pos_y+10)+'" opacity="0.7" font-size="10px" fill="black">'+fin_key+'</text> </g>'); //visibility="hidden"
				
				// Render Stuff in order to be able to get the text object in svg
				$(outer_div_id).html($(outer_div_id).html());
				
				// Change size and position of SVG text
			    var textNode = document.getElementById(id+'text');
			    var bb = textNode.getBBox();
			    var widthTransform = col_width / bb.width;
			    var heightTransform = pix_height / bb.height;
			    var value = widthTransform < heightTransform ? widthTransform : heightTransform;
				var font_height = value*bb.height*0.75;
				var text_width = value*bb.width*0.75;
				if(font_height > 3){
					textNode.setAttribute("font-size", String(parseInt(font_height))+"px");
					textNode.setAttribute("y", String(parseInt(pos_y+font_height/2+pix_height/2)));
					textNode.setAttribute("x", String(parseInt(pos_x+(col_width-text_width)/3)));
				}else{
					textNode.setAttribute("visibility", "hidden");
				}
								
				ix += 1;
		        pos_y = pos_y + pix_height;
		    }
		    pos_x = pos_x + col_width;
		    pos_y = 1;
	    }
	    $(outer_div_id).html($(outer_div_id).html());
	    
	    // Add to each of the boxes on hover functionality.
		$(document).ready(function() {
		    $('.'+div_class).hover(
		      function() { 
		      	$(this).css("opacity", 0.5); 
		      	id_this = $(this).attr('id');
		      	id_text = id_this+"text";
		      	$("#"+id_text).attr('opacity', 1);
			    /*
id_this = $(this).attr('id');
			    id_text = id_this+"text";
			    $("#"+id_text).attr('visibility', 'visible');
*/
			      
		      },
		      function() { 
		      
		      	$(this).css("opacity", 1); 
		      	id_this = $(this).attr('id');
		      	id_text = id_this+"text";
		      	$("#"+id_text).attr('opacity', 0.5);

			  /*  
id_this = $(this).attr('id');
			    id_text = id_this+"text";
			    $("#"+id_text).attr('visibility', 'hidden');  
*/
			      
		      }
		    );
		});
		
		
		// Make the tooltip work
	$(function() {
	
		    $( document ).tooltip({
		        track: true,
		        position: {
		            my: "left top+20 center",
		            at: "right center"
		        }
		    }); 
		    
		    $( "rect" ).tooltip({
		        items: ":not([disabled])",
		        content: function() {
		            var title = $( this ).find( "title" ),
		                content;
		            if ( title.length ) {
		                title.attr( "data-rect", $( this ).attr( "id" ) )
		                    .appendTo( "body" );
		            } else {
		                title = $( "[data-rect='" +
		                          $( this ).attr( "id" ) + "']" );
		            }
		            return title.html();
		        },
		        track: true, 
		        position: { 
		            my: "left top+20 center", 
		            at: "right center" } 
		    });
		    
		});
    }    
    
    function posistion_header(left_offset, box_width, header_id){
	    var outer_width = $(header_id).outerWidth();
	    $(header_id).css("left", left_offset + (math.abs(box_width)-outer_width)/2);
    }
    
    // Determine height positioning and font sizes
    max_box_height = math.max([w_rev, w_exp, w_ass]);
    space_left = inner_height - max_box_height*scale;
    space_left_top = space_left/2;
    font_h1 = space_left_top/4;
    font_h2 = space_left_top/6;
    
    $(".h_class").css("top", "10%");
    $(".statement_headings").css("top", "20px");
    $(".h_class").css("font-size", String(font_h2)+"px");
    $(".statement_headings").css("font-size", String(font_h1)+"px");
    
    
    // Scale and position revenue 
	rev_w_mod = scale*w_rev;
	var left_offset = math.subset(x, math.index(0,0));
	create_fin_rect(revenue, rev_w_mod, "#revenue_svg", "#income_div", left_offset, scale, "revenue_class", revenue_tot, revenue_color);
    posistion_header(left_offset, rev_w_mod, "#h_revenue");
	
	// Scale and position expenses
    exp_w_mod = scale*w_exp;
    left_offset = left_offset + rev_w_mod + math.subset(x, math.index(1,0));
    create_fin_rect(expenses, exp_w_mod, "#expense_svg", "#income_div", left_offset, scale, "expense_class", expenses_tot, expenses_color);
    posistion_header(left_offset, exp_w_mod, "#h_expenses");
	
	// Scale and position equal sign
	$("#h_equal").css("left", left_offset + exp_w_mod + math.subset(x, math.index(2,0))/2);
	
	// Scale and position Net income 
	net_w_mod = scale*w_net;
	left_offset = left_offset + exp_w_mod + math.subset(x, math.index(2,0));	
    create_fin_rect(net_income, net_w_mod, "#net_svg", "#income_div", left_offset, scale, "net_class", net_income_tot, net_income_color);
	posistion_header(left_offset, net_w_mod, "#h_net_income");
	
	// Scale and position Asset box
	var w_ass_mod = scale*w_ass;
	left_offset = math.subset(x, math.index(4,0))	
	create_fin_rect(assets, w_ass_mod, "#assets_svg", "#balance_div", left_offset, scale, "asset_class", tot_assets, assets_color);
	posistion_header(left_offset, w_ass_mod, "#h_assets");
	// Add bullet explainations
	$(".bullets").remove();
	$("#balance_div").append('<ul class="bullets" id="asset_bullets"><li class="bullets" id="curr_asset_bullet">Current Assets</li><li class="bullets" id="lt_asset_bullet">Long-Term Assets</li></ul>');
	$("#asset_bullets").css("left",String(left_offset)+"px");
	
	// Scale and position Liabilities box
	var w_lia_mod = scale*w_lia;
	left_offset = left_offset + w_ass_mod + math.subset(x, math.index(5,0))	
	create_fin_rect(liabilities, w_lia_mod, "#liabilities_svg", "#balance_div", left_offset, scale, "liabilities_class", liabilities_tot, liabilities_color);
	posistion_header(left_offset, w_lia_mod, "#h_liabilites");
	// Add bullet explainations
	$("#balance_div").append('<ul class="bullets" id="liab_bullets"><li class="bullets" id="curr_liab_bullet">Current Debt</li><li class="bullets" id="lt_liab_bullet">Long-Term Debt</li><li class="bullets" id="eqt_bullet">Equity</li></ul>');
	$("#liab_bullets").css("left",String(left_offset)+"px");
	
	// Scale and position CF Operations
	cf_op_w_mod = scale*w_cf_op;
	left_offset = math.subset(x, math.index(7,0));
    create_fin_rect(cf_operations, cf_op_w_mod, "#cf_operating_svg", "#cf_div", left_offset, scale, "cf_op_class", cf_operations_tot, cf_operations_color);
	posistion_header(left_offset, cf_op_w_mod, "#h_operating");
	
	// Scale and position CF Investing
	cf_inv_w_mod = scale*w_cf_inv;
	left_offset = left_offset + cf_op_w_mod + math.subset(x, math.index(8,0));
    create_fin_rect(cf_investing, cf_inv_w_mod, "#cf_investing_svg", "#cf_div", left_offset, scale, "cf_inv_class", cf_investing_tot, cf_investing_color);
	posistion_header(math.abs(left_offset), math.abs(cf_inv_w_mod), "#h_investing");
	
	// Scale and position CF Financing
	cf_fin_w_mod = scale*w_cf_fin;
	left_offset = left_offset +  cf_inv_w_mod + math.subset(x, math.index(9,0));
    create_fin_rect(cf_financing, cf_fin_w_mod, "#cf_financing_svg", "#cf_div", left_offset, scale, "cf_fin_class", cf_financing_tot, cf_financing_color);
	posistion_header(left_offset, cf_fin_w_mod, "#h_financing");
	
	// Scale and position Market Cap
	var w_mc_mod = scale*w_mc;
	left_offset = math.subset(x, math.index(11,0))	
	create_fin_rect(market_cap, w_mc_mod, "#market_cap_svg", "#m_cap_div", left_offset, scale, "m_cap_class", market_cap_tot, market_cap_color);
	
	if(ratio_mode != null){
		window[ratio_mode]();	
	}
}

positioning();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////      RATIOS      /////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function set_opacity_one(id_array){
	for(var id in id_array){
		$(id_array[id]).css("opacity", 1);
		$(id_array[id]+"text").css("opacity", 1);
	}
}

function pe(){
	whiten();
	set_opacity_one(["#0net_class","#0m_cap_class"]);
	pe_ = market_cap_tot/net_income_tot;
	var div = $('<div id="ratio_eq">$$P/E = \\frac{Price}{Net Income} = '+parseFloat(pe_).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "pe";
}
function pb(){
	whiten();
	set_opacity_one(["#0m_cap_class","#0asset_class", "#1asset_class", "#2asset_class", "#3asset_class", "#4asset_class","#5asset_class", "#6asset_class", "#7asset_class", "#8asset_class", "#9asset_class", "#curr_asset_bullet", "#lt_asset_bullet"]);
	pb_ = market_cap_tot/tot_assets;
	var div = $('<div id="ratio_eq">$$P/B = \\frac{Price}{Book Value} = '+parseFloat(pb_).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "pb";
}

function gross_profit_margin(){
	whiten();
	set_opacity_one(["#0expense_class","#0revenue_class"]);
	gpm = (revenue_tot+expenses[0]["Cost of Revenue"])/revenue_tot;
	var div = $('<div id="ratio_eq">$$Gross Profit Margin = \\frac{Total Sales - Cost of Revenue}{Total Sales} = '+parseFloat(gpm).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "gross_profit_margin";
}
function net_profit_margin(){
	whiten();
	set_opacity_one(["#0net_class","#0revenue_class"]);
	npm = net_income_tot/revenue_tot;
	var div = $('<div id="ratio_eq">$$Net Profit Margin = \\frac{Net Income}{Total Sales} = '+parseFloat(npm).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "net_profit_margin";
}
function roa(){
	whiten();
	set_opacity_one(["#0net_class","#0asset_class", "#1asset_class", "#2asset_class", "#3asset_class", "#4asset_class","#5asset_class", "#6asset_class", "#7asset_class", "#8asset_class", "#9asset_class", "#curr_asset_bullet", "#lt_asset_bullet"]);
	roa_ = net_income_tot/tot_assets;
	var div = $('<div id="ratio_eq">$$Return on Assets = \\frac{Net Income}{Total Assets} = '+parseFloat(roa_).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "roa";
}
function roe(){
	whiten();
	set_opacity_one(["#0net_class","#6liabilities_class","#7liabilities_class","#8liabilities_class", "#eqt_bullet"]);
	var eqt = 0;
	for(var key in liabilities[2]){
		eqt += liabilities[2][key];
	}
	roe_ = net_income_tot/eqt;
	var div = $('<div id="ratio_eq">$$Return on Equity = \\frac{Net Income}{Total Equity} = '+parseFloat(roe_).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "roe";
}
function debt_ratio(){
	whiten();
	set_opacity_one(["#0asset_class", "#1asset_class", "#2asset_class", "#3asset_class", "#4asset_class","#5asset_class", "#6asset_class", "#7asset_class", "#8asset_class", "#9asset_class", "#0liabilities_class","#1liabilities_class","#2liabilities_class", "#3liabilities_class","#4liabilities_class","#5liabilities_class", "#curr_asset_bullet", "#lt_asset_bullet", "#curr_liab_bullet", "#lt_liab_bullet"]);
	var current_liabilities = 0;
	for(var key in liabilities[0]){
		current_liabilities += liabilities[0][key];
	}
	var lt_liabilities = 0;
	for(var key in liabilities[1]){
		lt_liabilities += liabilities[1][key];
	}
	var debt_ratio_ = (lt_liabilities+current_liabilities)/tot_assets;
	var div = $('<div id="ratio_eq">$$Debt Ratio = \\frac{Total Liabilities}{Total Assets} = '+parseFloat(debt_ratio_).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "debt_ratio";
}
function debt_equity_ratio(){
	whiten();
	set_opacity_one(["#0liabilities_class","#1liabilities_class","#2liabilities_class", "#3liabilities_class","#4liabilities_class","#5liabilities_class","#6liabilities_class","#7liabilities_class","#8liabilities_class", "#lt_liab_bullet", "#curr_liab_bullet", "#eqt_bullet"]);
	var der = (liabilities_tot-eqt_tot)/eqt_tot;
	var div = $('<div id="ratio_eq">$$Debt to Equity Ratio = \\frac{Total Debt}{Total Equity} = '+parseFloat(der).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "debt_equity_ratio";
}
function current_ratio(){
	whiten();
	
	var current_assets = 0;
	for(var key in assets[0]){
		current_assets += assets[0][key];
	} 
	var current_liabilities = 0;
	for(var key in liabilities[0]){
		current_liabilities += liabilities[0][key]
	}
	
	var current_ratio_ = current_assets / current_liabilities;
	
	set_opacity_one(["#0asset_class", "#1asset_class", "#2asset_class", "#3asset_class", 
					"#4asset_class", "#0liabilities_class", "#1liabilities_class", "#2liabilities_class", "#curr_liab_bullet", "#curr_asset_bullet"]);

	var div = $('<div id="ratio_eq">$$Current Ratio = \\frac{Current Assets}{Current Liabilities} = '+parseFloat(current_ratio_).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "current_ratio";
}

function op_cf_rev(){
	whiten();
	set_opacity_one(["#0revenue_class", "#0cf_op_class", "#1cf_op_class", "#2cf_op_class"]);
	var op_cf_ = cf_operations_tot/revenue_tot;
	var div = $('<div id="ratio_eq">$$Operating CF/Revenue = \\frac{Operating Cash Flow}{Total Sales} = '+parseFloat(op_cf_).toFixed(2)+' $$</div>');
	$("#ratio_eq").remove();
	$("body").append(div);
	MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
	ratio_mode = "op_cf_rev";
}


function clear_ratio(){
	unwhiten();	
	$("#ratio_eq").remove()
}

// On window resizeing modify row height
$(window).resize(positioning);



$("#inner_container").css("fill", "blue");
$("#outer_container").height()